<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Laboratorio extends Model
{
    use HasFactory;
    protected $fillable = [
        'id',
        'nomdes',
        'nomcon',
        'cooid',
        'numcont',
        'fac',
        'facpab',
        'facpis',
        'tel',
        'eml',
        'des',
        'obj'
      ];
  
}
