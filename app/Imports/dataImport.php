<?php

namespace App\Imports;

use App\Models\Data;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToModel;

class dataImport implements ToModel

{
    /**
    * @param Collection $collection
    */
    public function model(array $row)
    {
        return new Data([
           'codigo_renacyt'     => $row[0],
           'investigador'    => $row[1], 
           'URL CTI Vitae'   => $row[4],
           'grupo'          => $row[5],
           'nivel'           => $row[6],
        ]);
    }
}
