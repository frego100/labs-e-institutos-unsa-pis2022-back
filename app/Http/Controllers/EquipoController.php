<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Equipo;
use App\Models\Laboratorio;

use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;
class EquipoController extends Controller
{
    public function crearEquipo()
    {
        // if(Auth::user()->hasPermissionTo('permisos_Admin')){
        return Inertia::render('crearEquipo',['id'=>Laboratorio::all()]);    
        // }
        // return redirect('/profile');
       
    }

    public function storeEquipo(Request $request){
        $image_path = '';
        if ($request->hasFile('img')) {
        $image_path = $request->file('img')->store('img', 'public');
        }
        //guarda en storage/app/public/img, sino pone : php artisan storage:link en consola
        Equipo::create(
            [
                'equi' => $request->equi,
                'ope' => $request->ope,
                'mantenimiento' => $request->mantenimiento,
                'img' => $image_path,
                'labcod' => $request->labcod,
            ]
        );
        
        return redirect('/mostrarEquipo');
    }
    public function deleteEquipo(Request $request){
        //por ID
        $dato = $request->equi;
        //console.log($dato);
        Equipo::findOrFail($dato)->delete();
        
         
        return redirect('/mostrarEquipo');
    }
    public function mostrarEquipo()
    {
        $equipos= Equipo::all();
        return Inertia::render('mostrarEquipo', ['equipos' => $equipos] );
       
    }
    public function borrarEquipo()
    {
        
        return Inertia::render('borrarEquipo');
       
    }
    public function editEquipo(Request $request){
        $equipo = Equipo::select('id', 'equi', 'ope', 'mantenimiento','img', 'labcod')
            ->find($request->input('index'));
        return Inertia::render('editEquipo', ['equipo'=>$equipo, 'id'=>Laboratorio::all()]);
    }
    public function saveEquipo(Request $request){
        $image_path = '';
        if ($request->hasFile('img')) {
        $image_path = $request->file('img')->store('img', 'public');
        }
        //guarda en storage/app/public/img, sino pone : php artisan storage:link en consola
        $equipo = Equipo::find($request->id);
        $equipo->update([
            'id' => $request->id,
            'equi' => $request->equi,
            'ope' => $request->ope,
            'mantenimiento' => $request->mantenimiento,
            'img' => $image_path,
            'labcod' => $request->labcod,
            
        ]);
        return redirect('mostrarEquipo');
    }

}
