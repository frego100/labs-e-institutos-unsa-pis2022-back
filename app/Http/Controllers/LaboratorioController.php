<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Models\Laboratorio;
use Illuminate\Http\Request;
use Inertia\Inertia;

class LaboratorioController extends Controller
{
  public function crearLab(){
    
      return Inertia::render('crearLab',['id'=>Laboratorio::all()]);
    
  }
  public function storeLab(Request $request){
    Laboratorio::create(
      [
        'nomdes' => $request->nomdes,
        'nomcon' => $request->nomcon,
        'cooid' => $request->cooid,
        'area' => $request->area,
        'fac' => $request->fac,
        'facpab' => $request->facpab,
        'facpis' => $request->facpis,
      ]
    );
    return redirect('indexLab');
  }
  public function indexlab(){
    if (Auth::user()->hasPermissionTo('permisos_Admin')){
      return Inertia::render('indexLab', ['labs'=>Laboratorio::all()]);
    }else{
      return redirect()->route('viewLab',['index'=> Auth::user()->codlab]);
    }
  }
  public function viewLab(Request $request){
    $lab = Laboratorio::select('*')
        ->find($request->input('index'));
    return Inertia::render('viewLab', ['lab'=>$lab]);
  }
  public function editLab(Request $request){
    $lab = Laboratorio::select('id', 'nomdes', 'nomcon', 'cooid', 'numcont', 'area', 'fac',
                               'facpab', 'facpis', 'tel', 'eml', 'des', 'obj')->find($request->input('index'));
    return Inertia::render('editLab', ['lab'=>$lab, 'id'=>Laboratorio::all()]);
  }
  public function saveLab(Request $request){
    $lab = Laboratorio::find($request->id);
    $lab->update([
        'id' => $request->id,
        'nomdes' => $request->nomdes,
        'nomcon' => $request->nomcon,
        'cooid' => $request->cooid,
        'numcont' => $request->numcont,
        'area' => $request->area,
        'fac' => $request->fac,
        'facpab' => $request->facpab,
        'facpis' => $request->facpis,
        'tel' => $request->tel,
        'eml' => $request->eml,
        'des' => $request->des,
        'obj' => $request->obj,
    ]);
    return redirect('indexLab');
  }
    
}
