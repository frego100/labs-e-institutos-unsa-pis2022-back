<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\List_user;
use App\Models\Laboratorio;
use Inertia\Inertia;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;

class ListUserController extends Controller
{
    public function newListUser()
    {
        
        return Inertia::render('newListUser',['role'=>Role::all(), 'lab'=>Laboratorio::select('id', 'nomdes')->get()]);    
        
    }

    public function saveListUser(Request $request){
        List_user::create(
            [
                'role_id' => $request->role_id,
                'email' => $request->email,
                'lab_id' => $request->lab_id,
            ]
        );
        return redirect('indexUser');
    }
}
