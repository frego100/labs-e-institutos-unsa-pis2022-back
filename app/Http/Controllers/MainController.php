<?php

namespace App\Http\Controllers;
use App\Models\User;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Reader\IReader;
use PhpOffice\PhpSpreadsheet\IOFactory;

class MainController extends Controller
{
    

    public function login(){
        if (Auth::check()) {

            return redirect('profile');
        }
        return Inertia::render('login');
    }

    public function profile(){
        
        $user = User::select('users.id', 'users.name as userName', 'surname', 'email', 'roles.name as roleName', 'dni', 'tel','avatar','codigo_renacyt','urlvitae','grupo','nivel')
            ->join('model_has_roles','model_has_roles.model_id','=','users.id')
            ->join('roles','model_has_roles.role_id','=','roles.id')
            ->find(Auth::user()->id);
        return Inertia::render('profile2', ['profile'=>$user]);
      
    }
    public function editProfile(){
        $user = User::select('users.id', 'users.name as userName', 'surname', 'email', 'dni', 'tel','roles.name as roleName','codigo_renacyt','urlvitae','grupo','nivel')
            ->join('model_has_roles','model_has_roles.model_id','=','users.id')
            ->join('roles','model_has_roles.role_id','=','roles.id')
            ->find(Auth::user()->id);
        return Inertia::render('editar_profile2', ['user'=>$user]);
    }
    public function saveProfile(Request $request){
        $user = User::find(Auth::user()->id);
        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load("../RENACYT.xlsx");
        
   
            $searchValue = $request->codigo_renacyt;
            $columna = 0;
            foreach ($spreadsheet->getActiveSheet()->getRowIterator() as $row) {
                $cellIterator = $row->getCellIterator();
                $cellIterator->setIterateOnlyExistingCells(true);
                foreach ($cellIterator as $cell) {
                    if ($cell->getValue() == $searchValue) {
                        $columna = $row->getRowIndex();
                        break;
                    }
                }      
            }

        $user->update([
            'name' => $request->name,
            'surname' => $request->surname,
            'dni' => $request->dni,
            'tel' => $request->tel,
            'codigo_renacyt'=> $spreadsheet->getActiveSheet()->getCellByColumnAndRow(1, $columna)->getValue(),
            'urlvitae'=>strval($spreadsheet->getActiveSheet()->getCellByColumnAndRow(4, $columna)->getValue()),
            'grupo' =>$spreadsheet->getActiveSheet()->getCellByColumnAndRow(5, $columna)->getValue()      ,
            'nivel' =>$spreadsheet->getActiveSheet()->getCellByColumnAndRow(6, $columna)->getValue() 

        ]);
        return redirect('profile');
    }
    public function indexUser(){
        if (Auth::user()->hasPermissionTo('permisos_Admin')){
            $users = User::select('users.id', 'users.name as userName', 'surname', 'email', 'roles.name as roleName', 'dni', 'tel','codlab')
            ->join('model_has_roles','model_has_roles.model_id','=','users.id')
            ->join('roles','model_has_roles.role_id','=','roles.id')
            ->get();
        }else{
            $users = User::select('users.id', 'users.name as userName', 'surname', 'email', 'roles.name as roleName', 'dni', 'tel','codlab')
            ->join('model_has_roles','model_has_roles.model_id','=','users.id')
            ->join('roles','model_has_roles.role_id','=','roles.id')
            ->where('codlab','=',Auth::user()->codlab)
            ->get();
        }
    return Inertia::render('indexUser', ['userList'=>$users,'role'=>Auth::user()]);
    }

    public function editUser(Request $request){
        $user = User::select('users.id', 'users.name as userName', 'surname', 'email', 'dni', 'tel')
            ->find($request->input('index'));
        return Inertia::render('editUser', ['userId'=>$user]);
    }

    public function saveUser(Request $request){
        $user = User::find($request->id);

        
        $user->update([
            'name' => $request->name,
            'surname' => $request->surname,
            'dni' => $request->dni,
            'tel' => $request->tel,
            

        ]);
        return redirect('indexUser');
    }
}
