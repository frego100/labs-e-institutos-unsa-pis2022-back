<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MainController;
use App\Http\Controllers\EquipoController;
use App\Http\Controllers\LaboratorioController;
use App\Http\Controllers\ListUserController;
use Laravel\Socialite\Facades\Socialite;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;
use App\Models\List_user;
use App\Imports\dataImport;
use Maatwebsite\Excel\Facades\Excel;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Reader\IReader;
use PhpOffice\PhpSpreadsheet\IOFactory;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login-google', function () {
       
    return Socialite::driver('google')->redirect();
});
 
Route::get('/google-callback', function () {
    $user = Socialite::driver('google')->stateless()->user();
    $role = List_user::where('email', $user->email)->first();
    
    if($role){
    
        $userExists = User::where('external_id', $user->id)->where('external_auth','google')->first();
        if($userExists){
            Auth::login($userExists);

            return redirect('indexUser');
            
            
        } else {
            
            $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load("../RENACYT.xlsx");
   
            $searchValue = $user->user['family_name'].', '.$user->user['given_name'];
            $columna = 0;
            foreach ($spreadsheet->getActiveSheet()->getRowIterator() as $row) {
                $cellIterator = $row->getCellIterator();
                $cellIterator->setIterateOnlyExistingCells(true);
                foreach ($cellIterator as $cell) {
                    if ($cell->getValue() == $searchValue) {
                        $columna = $row->getRowIndex();
                        break;
                    }
                }      
            }
            
            $newUser =User::create([
                'name' => $user->user['given_name'],
                'surname' => $user->user['family_name'],
                'email' => $user->email,
                'avatar' => $user->avatar,
                'external_id' => $user->id,
                'codlab' => $role->lab_id,
                'external_auth' => 'google',
                'codigo_renacyt'=> $spreadsheet->getActiveSheet()->getCellByColumnAndRow(1, $columna)->getValue(),
                'urlvitae'=>strval($spreadsheet->getActiveSheet()->getCellByColumnAndRow(4, $columna)->getValue()),
                'grupo' =>$spreadsheet->getActiveSheet()->getCellByColumnAndRow(5, $columna)->getValue()      ,
                'nivel' =>$spreadsheet->getActiveSheet()->getCellByColumnAndRow(6, $columna)->getValue()        

            ]);

            //Cierra Tais
            if ($role->role_id == 1){
                $newUser->assignRole('admin');
            }elseif($role->role_id == 2){
                $newUser->assignRole('operador');
            }else{
                $newUser->assignRole('coordinador');
            }

            

            Auth::login($newUser);

            return redirect('/editProfile');
        }
    }
    return redirect('/');
    
    

    
});

//Route::group(['middleware' => ['role:admin']], function () {
    
//});


Route::get('/', [MainController::class,'login'])->name('login');

Route::get('/profile',[MainController::class,'profile'])->middleware('auth');

Route::get('/editProfile',[MainController::class,'editProfile'])->middleware('auth');

Route::post('/saveProfile',[MainController::class, 'saveProfile'])->middleware('auth');

Route::get('/newListUser',[ListUserController::class,'newListUser'])->middleware('auth');

Route::post('/saveListUser',[ListUserController::class, 'saveListUser'])->middleware('auth');

Route::get('/logout',function(){
    Auth::logout();
    
    return redirect('/');
})->middleware('auth');

Route::get('indexUser',[MainController::class,'indexUser']);

Route::get('/editUser',[MainController::class,'editUser'])->middleware('auth')->name('editUser');

Route::post('/saveUser',[MainController::class, 'saveUser'])->middleware('auth');

Route::get('/crearEquipo',[EquipoController::class,'crearEquipo']);

Route::post('/storeEquipo',[EquipoController::class, 'storeEquipo'])->middleware('auth');

Route::get('/mostrarEquipo',[EquipoController::class, 'mostrarEquipo'])->middleware('auth');

Route::get('/borrarEquipo',[EquipoController::class, 'borrarEquipo'])->middleware('auth');

Route::post('/deleteEquipo',[EquipoController::class, 'deleteEquipo'])->middleware('auth');

Route::get('/editEquipo',[EquipoController::class, 'editEquipo'])->middleware('auth');

Route::post('/saveEquipo',[EquipoController::class, 'saveEquipo'])->middleware('auth');

Route::get('indexLab',[LaboratorioController::class,'indexLab'])->middleware('auth');

Route::get('/viewLab',[LaboratorioController::class,'viewLab'])->middleware('auth')->name('viewLab');

Route::get('/crearLab', [LaboratorioController::class,'crearLab']);

Route::post('/storeLab', [LaboratorioController::class, 'storeLab'])->middleware('auth');

Route::get('/editLab', [LaboratorioController::class, 'editLab'])->middleware('auth')->name('editLab');

Route::post('/saveLab',[LaboratorioController::class, 'saveLab'])->middleware('auth');

